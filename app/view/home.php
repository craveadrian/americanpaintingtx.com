<div id="content">
	<div id="welcome-section">
		<div class="row">
			<div class="left col-6 inbTop">
				<h2>WELCOME TO</h2>
				<h1>AMERICAN PAINTING</h1>
				<p>Painting seems like a relatively easy task, but there are actually several factors that can make it complicated. For one thing, you have to be familiar with color theory. Having knowledge of what colors match will help prevent you from getting results that clash. If you’re looking for a reliable paint contractor in Galveston, TX, American Painting is at your service. From your interior painting to your exteriors, let us take care of all your painting and drywall repair needs. We also offer an exceptional tile installation service.</p>
				<a href="<?php echo URL ?>about#content" class="btn">LEARN MORE</a>
				<a href="<?php echo URL ?>service#content" class="btn">CONTACT US</a>
				<div class="box1">
					<p>QUALITY PAINTING <span>AT AFFORDABLE PRICES</span> </p>
				</div>
			</div>
			<div class="right col-6 inbTop">
				<img src="public/images/content/welcome/img1.jpg" alt="Painting Men 1">
			</div>
			<img src="public/images/content/welcome/img3.jpg" alt="brush stroke" class="lessImg">
		</div>
	</div>
	<div id="reviews-section">
		<div class="row">
			<div class="left col-6 inbMid">
				<img src="public/images/content/reviews/img1.jpg" alt="House 1">
			</div>
			<div class="right col-6 inbMid">
				<h2>REVIEWS</h2>
				<h1>WHAT THEY SAY</h1>
				<p>Thank you very much. I’m impressed with your service. I’ve already told my friends about your company and your quick response, thanks again!</p>
				<p class="rvwAuthor">JOHN DOE <span>&#9733; &#9733; &#9733; &#9733; &#9733;</span> </p>
				<a href="<?php echo URL ?>about#content" class="btn">LEARN MORE</a>
				<a href="<?php echo URL ?>service#content" class="btn">CONTACT US</a>
			</div>
		</div>
	</div>
	<div id="our-service-section">
		<div class="row">
			<div class="ossTop">
				<div class="left col-7 inbBot">
					<h2>WHAT WE DO</h2>
					<h1>OUR SERVICES</h1>
				</div>
				<div class="right col-5 inbBot">
					<p class="social"> <a href="<?php $this->info('fb_link') ?>" class="socialico">f</a>
							<a href="<?php $this->info('fb_link') ?>" class="socialico">l</a>
							<a href="<?php $this->info('fb_link') ?>" class="socialico">x</a>
							<a href="<?php $this->info('fb_link') ?>" class="socialico">r</a>
					</p>
				</div>
			</div>
			<div class="ossBot">
				<a href="<?php echo URL ?>services#content"><div class="service-images col-3 inbTop">
					<img src="public/images/content/services/service1.jpg" alt="RESIDENTIAL PAINTING">
					<h2>RESIDENTIAL PAINTING</h2>
				</div></a>
				<a href="<?php echo URL ?>services#content"><div class="service-images col-3 inbTop">
					<img src="public/images/content/services/service2.jpg" alt="COMMERCIAL PAINTING">
					<h2>COMMERCIAL PAINTING</h2>
				</div></a>
				<a href="<?php echo URL ?>services#content"><div class="service-images col-3 inbTop">
					<img src="public/images/content/services/service3.jpg" alt="PRESSURE WASHING">
					<h2>PRESSURE WASHING</h2>
				</div></a>
				<a href="<?php echo URL ?>services#content"><div class="service-images col-3 inbTop">
					<img src="public/images/content/services/service4.jpg" alt="DRYWALL REPAIR">
					<h2>DRYWALL REPAIR</h2>
				</div></a>
			</div>
		</div>
	</div>
	<div id="contact-gallery-section">
		<div class="row">
			<div class="contact col-6 inbTop">
				<h2>KEEP IN TOUCH</h2>
				<h1>CONTACT US</h1>
				<p>We will be glad to answer your questions, feel free to ask a piece of information or a quotation. We are looking forward to work with you.</p>
				<form action="sendContactForm" method="post"  class="sends-email ctc-form" >
					<input type="text" name="name" placeholder="Name:">
					<input type="text" name="email" placeholder="Email:">
					<input type="text" name="phone" placeholder="Phone:">
					<textarea name="message" cols="30" rows="10" placeholder="Message:"></textarea>
					<div class="g-recaptcha"></div>
					<input type="checkbox" name="consent" class="consentBox inbMid"><p class="ckconsent inbMid">I hereby consent to having this website store my submitted information so that they can respond to my inquiry.</p><br>
					<?php if( $this->siteInfo['policy_link'] ): ?>
						<input type="checkbox" name="termsConditions" class="termsBox inbMid"/> <p class="ckconsent inbMid">I hereby confirm that I have read and understood this website's <a href="<?php $this->info("policy_link"); ?>" target="_blank">Privacy Policy.</a></p>
					<?php endif ?>
					<button type="submit" class="ctcBtn btn" disabled>SUBMIT FORM</button>
				</form>
				<div class="contactBot">
					<div class="col-11 inbMid">
						<div class="cnt-info"> <img src="public/images/common/phone.png" alt="Phone" class="inbMid"> <p class="phone inbMid"><?php $this->info(["phone","tel"]); ?></p> </div>
						<div class="cnt-info"> <img src="public/images/common/email.png" alt="Email" class="inbMid"> <p class="email inbMid"><?php $this->info(["email","mailto"]); ?></p> </div>
						<div class="cnt-info"> <img src="public/images/common/location.png" alt="Location" class="inbMid"> <p class="location inbMid"><?php $this->info("address"); ?></p> </div>
					</div>
					<div class="col-1 inbMid">
						<p class="social"> <a href="<?php $this->info('fb_link') ?>" class="socialico">f</a>
								<a href="<?php $this->info('fb_link') ?>" class="socialico">l</a>
								<a href="<?php $this->info('fb_link') ?>" class="socialico">x</a>
								<a href="<?php $this->info('fb_link') ?>" class="socialico">r</a>
						</p>
					</div>
				</div>
			</div>
			<div class="gallery col-6 inbTop">
				<h1>OUR GALLERY</h1>
				<div class="gallery-images">
					<img src="public/images/content/gallery/gallery1.jpg" alt="GALLERY 1">
					<img src="public/images/content/gallery/gallery2.jpg" alt="GALLERY 2">
					<img src="public/images/content/gallery/gallery3.jpg" alt="GALLERY 3">
					<img src="public/images/content/gallery/gallery4.jpg" alt="GALLERY 4">
					<img src="public/images/content/gallery/gallery5.jpg" alt="GALLERY 5">
					<img src="public/images/content/gallery/gallery6.jpg" alt="GALLERY 6">
				</div>
				<a href="<?php echo URL ?>gallery#content" class="btn">VIEW MORE</a>
			</div>
			<img src="public/images/content/gallery/stroke.jpg" alt="brush stroke" class="stroke">
		</div>
	</div>
	<div id="logo-section">
		<div class="row">
			<a href="<?php echo URL ?>"> <img src="public/images/common/botLogo.png" alt="American Painting Logo"> </a>
		</div>
	</div>
</div>
